#include "VirtualMachine.h"
#include "Machine.h"
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>
#include <cstring>
#include <cstdint>


using namespace std;

extern "C"{
	int ticksPerMS;
	volatile TVMTick tickCount = 0;
	const TVMMemoryPoolID VM_MEMORY_POOL_ID_SYSTEM = 0;
	const TVMMemoryPoolID VM_MEMORY_POOL_ID_SHARED = 1;

	struct Mutex;

	typedef struct TCB{
		TVMMemorySize memsize;
		TVMThreadPriority prio;
		TVMThreadEntry entry;
		volatile TVMThreadState state;
		TVMThreadID tid;
		void *param;
		void* stack;
		SMachineContextRef context;

		int flag;

		bool qUp = 0;

		bool waitingForIO;
		int IOResult = -1;

		volatile int mutexWait;

		Mutex* waitingOn;

		volatile int q;

		volatile unsigned int sleep;

		bool operator<(const TCB& rhs) const{
			if(flag == 1)
				return true;
			if(prio == rhs.prio)
				return q > rhs.q;
			return prio < rhs.prio;
		}
	} TCB;

	typedef struct Mutex{
		bool locked;
		TVMThreadIDRef owner;
		TVMMutexID mid;
		vector<TCB*> highQueue;
		vector<TCB*> midQueue;
		vector<TCB*> lowQueue;
	} Mutex;

	typedef struct MemoryBlock{
		uint8_t* start;
		uint8_t* end;
		unsigned int size;
	} MemoryBlock;

	typedef struct MemoryPool{
		unsigned int size;
		uint8_t* base;
		TVMMemoryPoolID mpid;

		vector<MemoryBlock> memoryBlocks;
	} MemoryPool;

	TVMMainEntry VMLoadModule(const char *module);
	TVMMainEntry VMUnloadModule();
	void machineFileWriteCallback(void* calledParam, int result);
	void machineAlarmCallback(void* callData);
	TCB* getThreadByID(TVMThreadID);
	void threadSkeleton(void* t);
	void scheduler();
	void idleEntry(void*);

	struct MyComparator {
  		bool operator() (TCB* arg1, TCB* arg2) {
    		return *arg1 < *arg2; //calls your operator
  		}
	};

	//Threading Stuff 
	Mutex* getMutexByID(TVMMutexID);
	void removeFromMutex(TCB*);
	vector<TCB*> threads;	
	priority_queue<TCB*, vector<TCB*>, MyComparator> readyQueue;
	TCB* current;

	volatile int q = 0;

	TVMThreadID nextTID = 1;

	//Mutex Stuff
	vector<Mutex*> muts;
	TVMMutexID nextMID = 1;
	void* sharedBase;

	//Memory Pool Stuff
	vector<MemoryPool*> memoryPools;
	TVMMemoryPoolID nextMPID = 1;

	TVMStatus VMStart(int tickms, TVMMemorySize heapSize, TVMMemorySize sharedSize, int argc, char *argv[]){
		sharedBase = MachineInitialize(sharedSize);

		ticksPerMS = tickms;
		MachineRequestAlarm(1000*tickms, &machineAlarmCallback, NULL);

		TVMMainEntry module = VMLoadModule(argv[0]);

		//Create main thread
		TCB* main = new TCB;
		main->state = VM_THREAD_STATE_RUNNING;
		main->prio = VM_THREAD_PRIORITY_NORMAL;
		main->sleep = 0;
		main->context = new SMachineContext;
		main->waitingForIO = 0;
		main->flag = 7;

		current = main;

		threads.push_back(main);

		MemoryPool* mainMemoryPool = new MemoryPool; 
		mainMemoryPool->mpid = VM_MEMORY_POOL_ID_SYSTEM;
		mainMemoryPool->size = heapSize;
		mainMemoryPool->base = new uint8_t[heapSize];
		memoryPools.push_back(mainMemoryPool);

		MemoryBlock start;
		start.end = mainMemoryPool->base;
		start.start = mainMemoryPool->base;
		start.size = 0;
		mainMemoryPool->memoryBlocks.push_back(start);
		MemoryBlock end;
		end.start = mainMemoryPool->base + mainMemoryPool->size;
		end.end = mainMemoryPool->base + mainMemoryPool->size;
		end.size = 0;
		mainMemoryPool->memoryBlocks.push_back(end);

		MemoryPool* sharedMem = new MemoryPool;
		sharedMem->mpid = nextMPID++;
		sharedMem->size = sharedSize;
		sharedMem->base = (uint8_t*)sharedBase;
		start.end = sharedMem->base;
		start.start = sharedMem->base;
		start.size = 0;
		sharedMem->memoryBlocks.push_back(start);
		end.start = sharedMem->base + sharedMem->size;
		end.end = sharedMem->base + sharedMem->size;
		end.size = 0;
		sharedMem->memoryBlocks.push_back(end);
		memoryPools.push_back(sharedMem);

		//Create idle thread
		TVMThreadID idleID;
		VMThreadCreate(idleEntry, NULL, 0x100000, VM_THREAD_PRIORITY_LOW, &idleID);
		VMThreadActivate(idleID);
		getThreadByID(idleID)->flag = 1;

		
		if(module){
			module(argc, argv);
			VMUnloadModule();
			MachineTerminate();
			return VM_STATUS_SUCCESS;
		}
		else
			return VM_STATUS_FAILURE;
	}

	TVMStatus VMThreadSleep(TVMTick tick){
		if(VM_TIMEOUT_INFINITE == tick)
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		current->state = VM_THREAD_STATE_WAITING;
		current->sleep = tick;

		if(VM_TIMEOUT_IMMEDIATE == tick){
			//////printf("sleep set to 0\n");
			current->sleep = 0; 
			current->state = VM_THREAD_STATE_READY;
			current->q = q++;
			readyQueue.push(current);	
		}
		
		scheduler();

		MachineResumeSignals(&state);

		return VM_STATUS_SUCCESS;
	}

	//Thread Commands
	TVMStatus VMThreadCreate(TVMThreadEntry entry, void *param, TVMMemorySize memsize, TVMThreadPriority prio, TVMThreadIDRef tid){
		TMachineSignalState state;
		MachineSuspendSignals(&state);		
		if(NULL == entry || NULL == tid){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}
		
		TCB *thread = new TCB;
		thread->prio = prio;
		thread->memsize = memsize;
		thread->entry = entry;
		thread->param = param;
		thread->state = VM_THREAD_STATE_DEAD;
		thread->sleep = 0;
		(*tid) = thread->tid = nextTID++;
		thread->flag = nextTID+100;
		thread->waitingForIO = 0;

		VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SYSTEM, thread->memsize, &thread->stack);

		threads.push_back(thread);	

		MachineResumeSignals(&state);
	
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMThreadDelete(TVMThreadID thread){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		TCB* t = getThreadByID(thread);
		if(NULL == t){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_ID;
		}
		if(VM_THREAD_STATE_DEAD != t->state){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_STATE;
		}
		VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SYSTEM, t->stack);
		threads.erase(std::remove(threads.begin(), threads.end(), t), threads.end());
		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMThreadActivate(TVMThreadID thread){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		TCB* t = getThreadByID(thread);
		
		if(NULL == t){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_ID;
		}
		
		if(VM_THREAD_STATE_DEAD != t->state){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_STATE;
		}

		t->state = VM_THREAD_STATE_READY;


		t->context = new SMachineContext;

		MachineContextCreate(t->context, threadSkeleton, t, t->stack, t->memsize);

		t->q = q++;

		readyQueue.push(t);

		scheduler();

		MachineResumeSignals(&state);

		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMThreadTerminate(TVMThreadID thread){
		TMachineSignalState state;
		MachineSuspendSignals(&state);

		TCB* t = getThreadByID(thread);


		if(t == NULL){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_ID;
		}

		if(t->state == VM_THREAD_STATE_DEAD){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_STATE;
		}

		t->state = VM_THREAD_STATE_DEAD;
		removeFromMutex(t);

		scheduler();

		MachineResumeSignals(&state);

		return VM_STATUS_SUCCESS;

	}

	TVMStatus VMThreadID(TVMThreadIDRef threadref){
		if(threadref != NULL){
			(*threadref) = current->tid;
			return VM_STATUS_SUCCESS;
		}
		else
			return VM_STATUS_ERROR_INVALID_PARAMETER;
	}

	TVMStatus VMThreadState(TVMThreadID thread, TVMThreadStateRef stateref){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		TCB* t = getThreadByID(thread);	

		if(NULL == stateref){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		if(t != NULL){
			(*stateref) = t->state;
			MachineResumeSignals(&state);
			return VM_STATUS_SUCCESS;
		}

		MachineResumeSignals(&state);

		return VM_STATUS_ERROR_INVALID_ID;
	}

	TCB* getThreadByID(TVMThreadID tid){
		for(auto it = threads.begin(); it != threads.end(); ++it){
			if(tid == (*it)->tid){
				return *it;
			}
		}
		return NULL;
	}

	TVMStatus VMTickMS(int *tickmsref){
		if(tickmsref != NULL){
			*tickmsref = ticksPerMS;
			return VM_STATUS_SUCCESS;
		}
		else
			return VM_STATUS_ERROR_INVALID_PARAMETER ;
	}

	TVMStatus VMTickCount(TVMTickRef tickref){
		if(tickref != NULL){
			(*tickref) = tickCount;
			return VM_STATUS_SUCCESS;
		}
		else
			return VM_STATUS_ERROR_INVALID_PARAMETER ;
	}

	void threadSkeleton(void* t){
		MachineEnableSignals();
		TCB* tr = (TCB*)t;
		tr->entry(tr->param);
		VMThreadTerminate(tr->tid);
	}

	void idleEntry(void * t){
		MachineEnableSignals();
		while(true){}
	}

	TVMStatus VMFileOpen(const char *filename, int flags, int mode, int *filedescriptor){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		current->state = VM_THREAD_STATE_WAITING;
		TCB *t = current;

		if (filename == NULL || filedescriptor == NULL){
			MachineResumeSignals (&state);
  			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		MachineFileOpen(filename, flags, mode, &machineFileWriteCallback, (void*)t);
		
		scheduler();

		*filedescriptor = t->IOResult;
		MachineResumeSignals(&state);
		if(*filedescriptor < 0){
			MachineResumeSignals(&state);
			return VM_STATUS_FAILURE;
		}


		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMFileRead(int filedescriptor, void *data, int *length){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		current->state = VM_THREAD_STATE_WAITING;
		TCB *t = current;

		if (data == NULL || length == NULL){
			MachineResumeSignals (&state);
  			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		void *mem;

		int written = 0;

		int i = 0;
		int j = *length;

		while(j > 512){
			while(VM_STATUS_ERROR_INSUFFICIENT_RESOURCES == VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, 512, &mem)){
				current->state = VM_THREAD_STATE_WAITING;
				scheduler();
			}
			MachineFileRead(filedescriptor, (uint8_t*)mem, 512, &machineFileWriteCallback, (void*)t);
			current->state = VM_THREAD_STATE_WAITING;
			scheduler();
			VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SHARED, mem);
			j -= 512;
			i += 512;
			written += t->IOResult;
			memcpy((uint8_t*)data + i, mem, 512);
		}

		while(VM_STATUS_ERROR_INSUFFICIENT_RESOURCES == VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, j, &mem)){
			current->state = VM_THREAD_STATE_WAITING;
			scheduler();
		}
		MachineFileRead(filedescriptor, (uint8_t*)mem, j, &machineFileWriteCallback, (void*)t);
		current->state = VM_THREAD_STATE_WAITING;
		scheduler();
		VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SHARED, mem);

		*length = t->IOResult + written;

		memcpy((uint8_t*)data + i, mem, j);

		MachineResumeSignals(&state);
		if(*length < 0){
			MachineResumeSignals (&state);
			return VM_STATUS_FAILURE;
		}


		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMFileWrite(int filedescriptor, void *data, int *length){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		current->state = VM_THREAD_STATE_WAITING;
		TCB *t = current;

		if (data == NULL || length == NULL){
			MachineResumeSignals (&state);
  			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		void *mem;

		int written = 0;

		int i = 0;
		int j = *length;
		while(j > 512){
			while(VM_STATUS_ERROR_INSUFFICIENT_RESOURCES == VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, 512, &mem)){
				current->state = VM_THREAD_STATE_WAITING;
				scheduler();
			}
			memcpy(mem, (uint8_t*)data + i, 512);
			MachineFileWrite(filedescriptor, (uint8_t*)mem, 512, &machineFileWriteCallback, (void*)t);
			current->state = VM_THREAD_STATE_WAITING;
			scheduler();
			VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SHARED, mem);
			j -= 512;
			i += 512;
			written += t->IOResult;
		}

		while(VM_STATUS_ERROR_INSUFFICIENT_RESOURCES == VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, j, &mem)){
			current->state = VM_THREAD_STATE_WAITING;
			scheduler();
		}
		memcpy(mem, (uint8_t*)data + i, j);
		MachineFileWrite(filedescriptor, (uint8_t*)mem, j, &machineFileWriteCallback, (void*)t);
		current->state = VM_THREAD_STATE_WAITING;
		scheduler();
		VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SHARED, mem);

		*length = t->IOResult + written;
		MachineResumeSignals(&state);
		if(*length < 0)
			return VM_STATUS_FAILURE;


		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMFileSeek(int filedescriptor, int offset, int whence, int *newoffset){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		current->state = VM_THREAD_STATE_WAITING;
		TCB *t = current;
		MachineFileSeek(filedescriptor, offset, whence, &machineFileWriteCallback, (void*)t);

		scheduler();
		

		*newoffset = t->IOResult;
		if(*newoffset < 0){
			MachineResumeSignals (&state);
			return VM_STATUS_FAILURE;
		}

		MachineResumeSignals(&state);

		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMFileClose(int filedescriptor){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		current->state = VM_THREAD_STATE_WAITING;
		TCB *t = current;
		MachineFileClose(filedescriptor, &machineFileWriteCallback, (void*)t);
		
		scheduler();

		MachineResumeSignals(&state);
		if(t->IOResult < 0){
			MachineResumeSignals (&state);
			return VM_STATUS_FAILURE;
		}

		return VM_STATUS_SUCCESS;
	}

	void scheduler(){
		if(readyQueue.empty())
			return;

		TCB* top = readyQueue.top();

		while(VM_THREAD_STATE_DEAD == top->state){
			readyQueue.pop();
			if(readyQueue.empty())
				return;
			else
				top = readyQueue.top();
		}

		if(top->prio > current->prio || (top->prio == current->prio && current->qUp) || current->state != VM_THREAD_STATE_RUNNING){
			TCB* old = current;
			readyQueue.pop();

			current->qUp = 0;
			current = top;


			if(old->state == VM_THREAD_STATE_RUNNING && current != old){
				old->state = VM_THREAD_STATE_READY;
				old->q = q++;
				readyQueue.push(old);
			}

			current->state = VM_THREAD_STATE_RUNNING;

			MachineContextSwitch(old->context, current->context);
		}
	}

	//Mutex commands

	TVMStatus VMMutexCreate(TVMMutexIDRef mutexref){
		if(mutexref == NULL)
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		
		Mutex* m = new Mutex;
		m->locked = false;
		m->owner = NULL;
		(*mutexref) = m->mid = nextMID++;
		muts.push_back(m);
		
		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;	
	}

	TVMStatus VMMutexDelete(TVMMutexID mutex){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		Mutex* m = getMutexByID(mutex);
		if(NULL == m){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_ID;
		}
		if(m->locked){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_STATE;
		}
		muts.erase(std::remove(muts.begin(), muts.end(), m), muts.end());
		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMMutexQuery(TVMMutexID mutex, TVMThreadIDRef ownerref){
		Mutex* m = getMutexByID(mutex);
		
		if(m == NULL)
			return VM_STATUS_ERROR_INVALID_ID;
		
		if(ownerref == NULL)
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		if(m->owner == NULL){
			(*ownerref) = VM_THREAD_ID_INVALID; 
		}
		else{
			(*ownerref) = (*m->owner);
		}

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;	
	}

	TVMStatus VMMutexAcquire(TVMMutexID mutex, TVMTick timeout){
		Mutex* m = getMutexByID(mutex);
		if(NULL == m)
			return VM_STATUS_ERROR_INVALID_ID;

		TMachineSignalState state;
		MachineSuspendSignals(&state);



		if(m->locked == false){
			m->locked = true;
			m->owner = &current->tid;
			current->waitingOn = m;
			MachineResumeSignals(&state);
			return VM_STATUS_SUCCESS;
		}
		else if(VM_TIMEOUT_IMMEDIATE == timeout){
			MachineResumeSignals(&state);
			return VM_STATUS_FAILURE;
		}
		
		int wait = timeout;

		if(VM_TIMEOUT_INFINITE == timeout){
			wait = -1;
		}
		
		current->mutexWait = wait;
		switch(current->prio){
			case VM_THREAD_PRIORITY_LOW:
				m->lowQueue.push_back(current);
				break;
			case VM_THREAD_PRIORITY_NORMAL:
				m->midQueue.push_back(current);
				break;
			case VM_THREAD_PRIORITY_HIGH:
				m->highQueue.push_back(current);
				break;
		}
		
		current->state = VM_THREAD_STATE_WAITING;
		current->mutexWait = wait;
		current->waitingOn = m;

		scheduler();

		if((*m->owner) == current->tid){
			MachineResumeSignals(&state);
			return VM_STATUS_SUCCESS;	
		}
		else{
			MachineResumeSignals(&state);
			return VM_STATUS_FAILURE;	
		}
	}

	TVMStatus VMMutexRelease(TVMMutexID mutex){
		Mutex* m = getMutexByID(mutex);
		if(m == NULL)
			return VM_STATUS_ERROR_INVALID_ID;

		TMachineSignalState state;
		MachineSuspendSignals(&state);

		current->waitingOn = NULL;
		////printf("res\n");
		
		if((*m->owner) == current->tid){
			m->owner = NULL;
			m->locked = false;
			if(!m->highQueue.empty()){
				TCB* owner = m->highQueue[0];	
				m->owner = &owner->tid;
				m->highQueue.erase(m->highQueue.begin());
				owner->mutexWait = 0;
				owner->state = VM_THREAD_STATE_READY;
				owner->q = q++;
				readyQueue.push(owner);
			}
			else if(!m->midQueue.empty()){
				TCB* owner = m->midQueue[0];	
				m->owner = &owner->tid;
				m->midQueue.erase(m->midQueue.begin());
				owner->mutexWait = 0;
				owner->state = VM_THREAD_STATE_READY;
				owner->q = q++;
				readyQueue.push(owner);
			}
			else if(!m->lowQueue.empty()){
				TCB* owner = m->lowQueue[0];	
				m->owner = &owner->tid;
				m->lowQueue.erase(m->lowQueue.begin());
				owner->mutexWait = 0;
				owner->state = VM_THREAD_STATE_READY;
				owner->q = q++;
				readyQueue.push(owner);
			}
			else{
				m->owner = NULL;
				m->locked = false;
			}

			scheduler();
		}
		else{
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_STATE;		
		}

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;	
	}

	Mutex* getMutexByID(TVMMutexID mid){
		for(auto it = muts.begin(); it != muts.end(); ++it){
			if(mid == (*it)->mid){
				return *it;
			}
		}
		return NULL;
	}

	//Pool Stuff

	void insertBlock(MemoryPool *pool, MemoryBlock block){
		for(auto it = pool->memoryBlocks.begin(); it != pool->memoryBlocks.end(); ++it){
			if(block.start < (*it).start){
				pool->memoryBlocks.insert(it, block);
				return;
			}
		}
		//printf("This is bad\n");
	}

	MemoryPool* getMemoryPoolByID(TVMMemoryPoolID mpid){
		for(auto it = memoryPools.begin(); it != memoryPools.end(); ++it){
			if(mpid == (*it)->mpid){
				return *it;
			}
		}
		return NULL;
	}

	uint8_t* findSpace(MemoryPool* pool, MemoryBlock block){
		uint8_t* startPoint = NULL;

		//printf("%d: %d count\n", pool->mpid, (int)pool->memoryBlocks.size());

		for(unsigned int j = 0; j < pool->memoryBlocks.size(); j++){
			//printf("%p to %p\n", pool->memoryBlocks[j].start, pool->memoryBlocks[j].end);
		}

		unsigned int i = 0;
		for(; i < pool->memoryBlocks.size()-1; i++){
			if(pool->memoryBlocks[i+1].start - pool->memoryBlocks[i].end >= block.size){
				startPoint = pool->memoryBlocks[i].end;
				break;
			}
		}
		return startPoint;
	}

	TVMStatus VMMemoryPoolCreate(void *base, TVMMemorySize size, TVMMemoryPoolIDRef memory){
		if(base == NULL || size == 0 || memory == NULL)
			return VM_STATUS_ERROR_INVALID_PARAMETER;

		TMachineSignalState state;
		MachineSuspendSignals(&state);

		MemoryPool *pool = new MemoryPool;
		pool->mpid = nextMPID++;
		pool->size = size;
		pool->base = (uint8_t*)base;
		memoryPools.push_back(pool);

		*memory = pool->mpid;

		MemoryBlock block;
		block.end = pool->base;
		block.start = pool->base;
		block.size = 0;

		pool->memoryBlocks.push_back(block);

		MemoryBlock end;
		end.size = 0;
		end.start = pool->base + pool->size;
		end.end = pool->base + pool->size;
		
		pool->memoryBlocks.push_back(end);

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMMemoryPoolDelete(TVMMemoryPoolID memory){
		TMachineSignalState state;
		MachineSuspendSignals(&state);

		MemoryPool *pool = getMemoryPoolByID(memory);

		if(pool == NULL){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		if(pool->memoryBlocks.size() > 2){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_STATE;
		}

		memoryPools.erase(std::remove(memoryPools.begin(), memoryPools.end(), pool), memoryPools.end());

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMMemoryPoolQuery(TVMMemoryPoolID memory, TVMMemorySizeRef bytesleft){
		if(bytesleft == 0)
			return VM_STATUS_ERROR_INVALID_PARAMETER;

		TMachineSignalState state;
		MachineSuspendSignals(&state);

		MemoryPool *pool = getMemoryPoolByID(memory);

		if(pool == NULL){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		*bytesleft = pool->size;
		for(auto it = pool->memoryBlocks.begin(); it != pool->memoryBlocks.end(); ++it){
			*bytesleft -= (*it).size;
		}

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMMemoryPoolAllocate(TVMMemoryPoolID memory, TVMMemorySize size, void **pointer){
		if(size == 0 || pointer == NULL)
			return VM_STATUS_ERROR_INVALID_PARAMETER;

		TMachineSignalState state;
		MachineSuspendSignals(&state);

		MemoryPool *pool = getMemoryPoolByID(memory);

		if(pool == NULL){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		if(size%64 != 0)
			size = (size + 64) - (size + 64)%64;

		MemoryBlock block;
		block.size = size;
		block.start = findSpace(pool, block);

		if(block.start == NULL){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INSUFFICIENT_RESOURCES;
		}

		block.end = block.start+block.size;

		insertBlock(pool, block);
		*pointer = block.start;


		//printf("Allocating in %p to %p\n", block.start, block.end);

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMMemoryPoolDeallocate(TVMMemoryPoolID memory, void *pointer){
		if(pointer == NULL)
			return VM_STATUS_ERROR_INVALID_PARAMETER;

		TMachineSignalState state;
		MachineSuspendSignals(&state);

		MemoryPool *pool = getMemoryPoolByID(memory);

		if(pool == NULL){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		bool found = false;
		for(auto it = pool->memoryBlocks.begin()+1; it != pool->memoryBlocks.end()-1; ++it){
			if(pointer == (*it).start){
				pool->memoryBlocks.erase(it);
				//printf("Deallocating in %p\n", (*it).start);
				found = true;
				break;
			}
		}

		if(!found){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}


	void machineFileWriteCallback(void* calledParam, int result){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		((TCB*)calledParam)->IOResult = result;
		((TCB*)calledParam)->state = VM_THREAD_STATE_READY;
		((TCB*)calledParam)->q = q++;
		readyQueue.push((TCB*)calledParam);
		MachineResumeSignals(&state);
	}

	void removeFromMutex(TCB* t){
		////printf("%d\n", t->tid);
		if(t->waitingOn != NULL){
			Mutex* m = t->waitingOn;
			if(NULL != m->owner && (*m->owner) == t->tid){
				m->owner = NULL;
				m->locked = false;
			}
			switch(t->prio){
				case VM_THREAD_PRIORITY_LOW:
					m->lowQueue.erase(std::remove(m->lowQueue.begin(), m->lowQueue.end(), t), m->lowQueue.end());
					break;
				case VM_THREAD_PRIORITY_NORMAL:
					m->midQueue.erase(std::remove(m->midQueue.begin(), m->midQueue.end(), t), m->midQueue.end());
					break;
				case VM_THREAD_PRIORITY_HIGH:
					m->highQueue.erase(std::remove(m->highQueue.begin(), m->highQueue.end(), t), m->highQueue.end());
					break;
			}
		}
	}

	void machineAlarmCallback(void* callData){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		tickCount++;
		current->qUp = 1;
		for(auto it = threads.begin(); it != threads.end(); ++it){
			if(VM_THREAD_STATE_WAITING == (*it)->state){
				if((*it)->mutexWait > 0 && --(*it)->mutexWait == 0){
					(*it)->state = VM_THREAD_STATE_READY;
					(*it)->q = q++;
					removeFromMutex((*it));
					readyQueue.push((*it));
				}
				else if((*it)->sleep != 0 && --(*it)->sleep == 0){
					(*it)->state = VM_THREAD_STATE_READY;
					(*it)->q = q++;
					readyQueue.push((*it));
				}
			}
		}
		scheduler();
		MachineResumeSignals(&state);
	}
}
